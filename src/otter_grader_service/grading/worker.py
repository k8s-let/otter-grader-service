import random
import os
from otter_grader_service.common.logging import logger
import shutil
import base64
import zipfile
from io import BytesIO
from pydantic import BaseModel
from otter_grader_service.common.payload import Submission, Grading
from otter_grader_service.common.configuration import grading_home, grading_image

# Job status codes
GRADING_OK = 0
MULTIPLE_NOTEBOOKS = 1
NO_NOTEBOOK = 2
NO_RESULTS = 3
NO_JSON = 4
NO_PDF = 5
PERMISSION_DENIED = 6
OTHER_ERROR = 99

grading_info_file = "grading.zip"

log = logger(__name__)


class Job(BaseModel):
    """
    Job for one submission to grade.
    """

    id: int = random.randrange(
        10000000, 99999999
    )  # nosec B311  random ID for working dir creation
    status: int = GRADING_OK
    image: str = grading_image  # Docker image to be used
    submission: Submission  # Submission and grading information
    grading: Grading = None  # Result to be returned to the LMS
    notebook_name: str = ""  # name of notebook extracted from submission
    log: str = ""


class Worker(BaseModel):
    """
    Base class for workers doing the actual grading.
    """

    grading_dir: str = "grade"  # Mounted working directory inside Docker container

    def __init__(self, working_space_root=grading_home, current_job=None):
        """
        Initialize new worker. Parameters are mainly for test runners. Default (*configuration.grading_home*)
        is set to `/home/jovyan` which exists in Jupyter notebook images used for grading. *current_job* will
        be set to the job got from the queue message, it can be explicitly set for unit tests.

        :param working_space_root: Where the root grading directory is
        :param current_job: Initialize with a job, for testing
        """
        BaseModel.__init__(self)
        self.working_space_root = working_space_root
        self.current_job = current_job

    def grade(self, job: Job):
        """
        Do the heavy lifting. Accepts a job, performs the grading and reads the results.

        :param job: Job class instance
        """
        raise NotImplementedError

    def grading_path(self):
        """
        Construct a path from the configured root directory by adding an intermediate directory
        'grade' for temporary grading directories and a job specific ID.
        """
        return os.path.join(
            self.working_space_root, self.grading_dir, "job_" + str(self.current_job.id)
        )

    def prepare_dir(self):
        """
        Create the temporary working directory.
        """
        try:
            os.makedirs(self.grading_path())
            log.debug(f"Directory created: {self.grading_path()}")
        except FileExistsError:
            log.warning(f"Grading directory exists: {self.grading_path()}. Removing.")
            shutil.rmtree(self.grading_path(), ignore_errors=True)
            os.mkdir(os.path.join(self.grading_path()))
            log.debug(f"Directory created after cleanup: {self.grading_path()}")
        except PermissionError:
            log.error(f"No permission to write to {self.grading_path()}.")
            self.current_job.status = PERMISSION_DENIED
        except Exception as e:
            log.error(f"Error during working directory preparation: {e}")
            self.current_job.status = OTHER_ERROR
        else:
            self.current_job.status = GRADING_OK
        try:
            # Make sure Docker container can read and write
            os.chmod(self.grading_path(), 0o777)  # nosec B103
        except Exception as e:
            log.warn(e)

    def write_grading_info(self):
        """
        Write file containing grading info and solutions
        """
        f = open(os.path.join(self.grading_path(), grading_info_file), "wb")
        f.write(base64.b64decode(self.current_job.submission.solution))
        f.close()
        log.debug(
            f"Grading ZIP written for job {self.current_job.submission.submission_id}"
        )

    def check_submission(self):
        """
        Test submission: exactly one notebook is allowed
        """
        notebook = ""
        notebook_seen = False
        for file in os.listdir(self.grading_path()):
            if not notebook_seen and file.endswith(".ipynb"):
                notebook_seen = True
                notebook = file
            elif file.endswith(".ipynb"):
                self.current_job.status = MULTIPLE_NOTEBOOKS
                log.error(
                    f"Multiple notebooks for submission {self.current_job.submission.submission_id}"
                )
                return
        if notebook_seen:
            self.current_job.notebook_name = os.path.splitext(notebook)[0]
            self.current_job.status = GRADING_OK
        else:
            self.current_job.status = NO_NOTEBOOK
            log.error(
                f"No notebooks for submission {self.current_job.submission.submission_id}"
            )

        log.debug(
            f"Submission check ok for submission {self.current_job.submission.submission_id}"
        )

    def read_grading(self):
        """
        Read grading result files: results.json with scores and a rendered notebook.pdf
        """
        zip_blob = BytesIO()
        pwd = os.getcwd()
        os.chdir(self.grading_path())
        # Create ZIP with result files
        with zipfile.ZipFile(zip_blob, "w", zipfile.ZIP_DEFLATED) as z:
            try:
                z.write("results.json")
            except FileNotFoundError as e:
                log.error("Results file not found: {0}".format(e))
                self.current_job.status = NO_JSON
            try:
                z.write(self.current_job.notebook_name + ".pdf")
            except FileNotFoundError as e:
                log.error("PDF not found: {0}".format(e))
                if self.current_job.status == NO_JSON:
                    self.current_job.status = NO_RESULTS
                else:
                    self.current_job.status = NO_PDF

        if self.current_job.status == GRADING_OK:
            # rewind file and read for submission to queue message
            zip_blob.seek(0)
            self.current_job.grading.results = base64.b64encode(zip_blob.read())
            zip_blob.close()
            os.chdir(pwd)
            log.debug(
                f"Results imported for submission {self.current_job.submission.submission_id}"
            )
        else:
            self.current_job.grading.results = ""

    def extract_submission(self):
        """
        Extract the archive containing the submission and auxiliary files (data etc.)
        """
        zip_blob = BytesIO(
            initial_bytes=base64.b64decode(self.current_job.submission.submission)
        )
        z = zipfile.ZipFile(zip_blob, "r")
        z.extractall(self.grading_path())
        z.close()
        log.debug(
            f"Results imported for submission {self.current_job.submission.submission_id}"
        )

    def finalize(self):
        """
        Set job metadata and remove remains of grading process
        """
        self.current_job.grading.rc = self.current_job.status
        self.current_job.grading.submission_id = (
            self.current_job.submission.submission_id
        )
        self.current_job.grading.course_id = self.current_job.submission.course_id

        shutil.rmtree(self.grading_path(), ignore_errors=True)

        log.debug(f"Job finalized for {self.current_job.submission.submission_id}")
