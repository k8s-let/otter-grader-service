FROM python:3.11-alpine
LABEL authors="bgiger"

LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"
ENV PYTHONUNBUFFERED=1

# Use artifacts created in previous step
COPY dist/* .
RUN pip install *.whl

RUN addgroup py && adduser -D -G py py
USER py:py

EXPOSE 8000
#CMD gunicorn -b 0.0.0.0:8000 -t 120 -w 4 --worker-class uvicorn.workers.UvicornWorker otter_grader_service.pusher_main:app
CMD gunicorn -b 0.0.0.0:8000 -t 120 -w 4 --worker-class uvicorn.workers.UvicornWorker otter_grader_service.pusher.main:app
