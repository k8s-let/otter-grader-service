import unittest
import base64
import json
import hashlib
import time
from fastapi import HTTPException
from otter_grader_service.common.payload import SecureMessage, Submission

dummy_payload = Submission(
    rc=0,
    status="OK",
    submission_id=9876,
    course_id=22222,
    solution="aaaaaaaaaaa",
    submission="bbbbbbbbbbb",
)


# Prepare payload for comparision, do the nasty remove-timestamp-fix
js = json.loads(dummy_payload.model_dump_json())
del js["timestamp"]
md5_correct = base64.b64encode(
    hashlib.md5(
        json.dumps(js, separators=(",", ":")).encode("utf-8")
    ).digest()  # noseq B303
).decode("utf-8")

md5_incorrect = base64.b64encode(
    hashlib.md5("Not valid".encode("utf-8")).digest()  # noseq B303
).decode("utf-8")

msg = SecureMessage(  # noseq B106
    payload=dummy_payload,
    md5_payload=md5_correct,
    secret="1",
    rc=0,
    msg="OK",
    timestamp=int(time.time()),
    auth="not",
)


class EncoderTests(unittest.TestCase):
    def test_checksum_incoming(self):
        assert md5_correct == msg.payload.md5_sum().decode("utf-8")
        assert md5_incorrect != msg.payload.md5_sum().decode("utf-8")
        msg.md5_payload = md5_correct
        assert msg.validate_checksum()
        msg.md5_payload = md5_incorrect
        self.assertRaises(HTTPException, msg.validate_checksum)

    def test_ttl_incoming(self):
        # return
        self.assertTrue(msg.validate_timestamp())
        time.sleep(10)
        self.assertRaises(HTTPException, msg.validate_timestamp)
