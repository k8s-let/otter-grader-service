# otter-grader-service: Services for Jupyter notebook grading.

**Repository has moved!** New repo: https://gitlab.ethz.ch/k8s-let/grading-service/otter-grader-service

## Summary
*otter-grader-service* provides several services for automatic grading of JupyterNotebooks.

The process of autograding uses *otter-grader* to perform the grading. Notebooks have to
be prepared for *otter-grader* and split by `otter generate` into a student version and
a `autograder.zip` containing grading information.

Students get the student version of the notebooks as assignments, and return their solutions
as submission. A submission, together with the `autograder.zip`, will be fed into the grading
process.

The services involved in the grading process are

- a LMS handling assignments, submissions and grading feedback to students and teacher
- an API grading server which receives submissions and the `autograder.zip`
- a messaging system buffering grading jobs and grading results
- a grading worker which consumes the job messages, grades the submission and enqueues the results
- a responder which takes the results and returns them to the LMS

This repository contains the code for the API service, the grading worker for a host capable of running
Docker and the responder.

## Installation
### Common Configuration Options
Configuration options for the services published here use environment variables. Options common to all
services are:

- `LOGLEVEL`: 'INFO', 'DEBUG', 'WARN', as usual
- `CERT_DIRECTORY`: path to TLS certificate files.
- `DEPLOYMENT_TYPE`: i.e. 'dev', 'test', 'qa', 'prod'. Effects also the suffix used for message vhost and queue.
- ...


### RabbitMQ
Install an instance of the message queue system *RabbitMQ* as described in the
[RabbitMQ documentation](https://www.rabbitmq.com/admin-guide.html). See also the documention
[in the conf.d](conf.d/README.md) directory for configuration examples.

### Grading Server

### Grader

### Responder
