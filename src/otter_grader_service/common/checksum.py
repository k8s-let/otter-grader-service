import base64
import hashlib
import hmac
from .configuration import key, secret
import json


def calc_md5_test(data):
    # TODO: what has PHP/Moodle to offer as hash algo? BLAKE2b?
    return base64.b64encode(hashlib.md5(data.encode("utf-8")).digest())  # nosec B324


def calc_md5(payload):
    """
    Calculate MD5 checksum
    :param payload: Source to calculate the checksum from. Checksum is calculated on a JSON string without whitespace.
    :return: MD5 checksum
    """
    js = json.loads(payload.model_dump_json())
    # Not so elegant fix: remove internally added timestamp for checksum calculation;
    # was not present when request was generated in LMS
    del js["timestamp"]
    # Get rid of whitespace in pydantic json
    js = json.dumps(js, separators=(",", ":"))
    return base64.b64encode(hashlib.md5(js.encode("utf-8")).digest())  # nosec B324


def calc_signature(timestamp, md5_payload):
    """
    Calculate the HMAC-SHA256 key for authentication
    :param timestamp: timestamp set in request
    :param md5_payload: md5 sum of payload
    :return: calculated key
    """
    string = key + timestamp + md5_payload
    encoded_hmac = base64.b64encode(
        hmac.new(
            secret.encode("utf-8"), string.encode("utf-8"), digestmod=hashlib.sha256
        ).digest()
    )
    return encoded_hmac.decode("utf-8")
