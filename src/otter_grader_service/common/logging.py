import logging
import os


def logger(name):
    """
    Custom log format
    :param name: logger name displayed in output
    :return:
    """
    logging.basicConfig(
        level=os.environ.get("LOGLEVEL", "INFO").upper(),
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )
    mylog = logging.getLogger(name)
    return mylog
