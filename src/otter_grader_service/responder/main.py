from importlib.metadata import version

__version__ = version("otter_grader_service")
import sys
import time
import logging
from otter_grader_service.common.configuration import (
    moodle_host,
    key,
    secret,
    stay_connected,
)
from otter_grader_service.common.logging import logger
from otter_grader_service.common.queues import subscribe_results
from otter_grader_service.common.payload import Grading, SecureMessage
from otter_grader_service.common.checksum import calc_md5, calc_signature

moodle_url = "https://" + moodle_host + "/push?submission_id="

logging.getLogger("pika").setLevel(logging.WARNING)
log = logger("Responder")
log.info(f"Otter grader service v{__version__}")

if key is None or secret is None:
    log.critical("Auth key/secret missing.")
    exit(1)


def main():
    """
    Listen to results queue, process messages and forward to LMS
    :return:
    """

    def callback(ch, method, properties, body):
        """
        Process an incoming payload.Grading message
        :param ch: RabbitMQ Channel
        :param method:
        :param properties:
        :param body: Message body (the Grading object)
        :return:
        """

        grading = Grading.model_validate_json(body)
        # trip time measuring
        delta = time.time() - grading.timestamp
        if grading.submission_id == 0:
            log.info(f"Ping received, time elapsed {delta}s")
        else:
            log.info(
                f"Received results for submission {grading.submission_id}, time elapsed {delta}s"
            )
            md5sum = calc_md5(grading).decode("utf-8")
            timestamp = str(int(time.time()))

            # Message object to be sent back to LMS
            msg = SecureMessage(
                payload=grading,
                md5_payload=md5sum,
                auth=calc_signature(timestamp, md5sum),
                timestamp=timestamp,
                rc=0,
                msg="OK",
            )

            log.info(moodle_url + str(msg.payload.submission_id))

        ch.basic_ack(delivery_tag=method.delivery_tag)

    subscribe_results(callback)
    if stay_connected == "on":
        while True:
            subscribe_results(callback)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        log.info("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            exit(0)
