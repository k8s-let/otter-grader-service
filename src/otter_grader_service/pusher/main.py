from importlib.metadata import version

__version__ = version("otter_grader_service")
import time
from typing import Annotated
from fastapi import FastAPI, Header
from pika.exceptions import AMQPConnectionError
from apscheduler.schedulers.background import BackgroundScheduler
import uvicorn
import asyncio
import logging

from ..common.logging import logger
from ..common.configuration import key, secret
from ..common.payload import Submission, SecureMessage, TokenMessage
from ..common.queues import push_job

SUBMISSION_OK = 0
OTHER_ERROR = 1
NO_QUEUE_CONNECTION = 100

log = logger("root")
logging.getLogger("pika").setLevel(logging.WARNING)
if key is None or secret is None:
    log.critical("Auth key/secret missing.")
    exit(1)

app = FastAPI()
scheduler = BackgroundScheduler()
scheduler.start()
log.info(f"Otter grader service v{__version__}")


def ping_queue():
    """
    Keep connection up.
    """
    log.info("Ping queue")
    # empty submission for keepalive ping
    ping_data = Submission(
        submission_id=0, course_id=0, solution="", submission="", timestamp=time.time()
    )
    push_job(ping_data)


scheduler.add_job(ping_queue, "interval", seconds=60, id="ping_queue")


async def exit_app():
    loop = asyncio.get_running_loop()
    loop.stop()


@app.get("/")
async def root():
    """
    Human-readable status message.
    """
    return {"status": "Grading server running"}


@app.get("/healthz")
async def healthz():
    """
    Computer readable status message.
    """
    return {"status": 0}


@app.post("/submit/")
async def submit(submission: Submission, ts: int, md5sum: str, key: str):
    """
    Accepts  for grading
    :param submission: type payload.Submission
    :param ts: Timestamp set by sender
    :param md5sum: checksum over payload (submission)
    :param key: shared key (AUTH_KEY)
    :return: status code in JSON
    """
    log.debug(f"Got msg: submission_id {submission.submission_id}")
    msg = SecureMessage(payload=submission, md5_payload=md5sum, auth=key, timestamp=ts)
    msg.validate_all()
    log.debug("Message validated")
    try:
        push_job(msg.payload)
        log.info(f"Pushed grading for submission {msg.payload.submission_id}")
        return {"return": {"code": SUBMISSION_OK, "status": "ok"}}
    except AMQPConnectionError:
        log.error("Could not connect to queue service")
        return {
            "return": {
                "code": NO_QUEUE_CONNECTION,
                "status": "Could not connect to queue service",
            }
        }


@app.post("/push/")
async def submit_with_token(
    submission: Submission, access_token: Annotated[str | None, Header()] = None
):
    """
    Accepts  for grading
    :param submission: type payload.Submission
    :param access_token: client access token (GET_TOKEN)
    :return: status code in JSON
    """
    log.debug(f"Got msg: submission_id {submission.submission_id}")
    msg = TokenMessage(payload=submission, token=access_token)
    msg.validate_all()
    log.debug("Message validated")
    try:
        push_job(msg.payload)
        log.info(f"Pushed grading for submission {msg.payload.submission_id}")
        return {"return": {"code": 0, "status": "ok"}}
    except Exception as e:
        log.error(e)
        return {"return": {"code": 1, "status": "failed"}}


if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=8000)
