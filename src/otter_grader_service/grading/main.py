from importlib.metadata import version

__version__ = version("otter_grader_service")
import logging
import os
import signal

from otter_grader_service.common.configuration import stay_connected
from otter_grader_service.common.logging import logger
from otter_grader_service.common.queues import push_result, subscribe_jobs, unsubscribe
from otter_grader_service.common.payload import Submission, Grading
from otter_grader_service.grading.worker import Job
from otter_grader_service.grading.worker_docker import WorkerDocker

log = logger("root")
logging.getLogger("pika").setLevel(logging.WARNING)
log.info(f"Otter grader service v{__version__}")

# Flag to allow a graceful exit, evaluated during callback
consume_messages = True


def signal_received(signum, frame):
    """
    Allow graceful exit from queue, avoid a hard exit during handling of a grading job.
    :param signum:
    :param frame:
    :return:
    """
    global consume_messages
    # Set flag and unsubscribe from queue
    consume_messages = False
    unsubscribe()


signal.signal(signal.SIGINT, signal_received)


def main():
    """
    Listen to grading queue. Read submissions, pack into a grading job and submit to the worker
    for grading.
    :return:
    """
    worker = WorkerDocker()
    if not os.path.exists(worker.working_space_root):
        log.fatal("Directory {0} not found".format(worker.working_space_root))
        exit(1)

    def callback(ch, method, properties, body):
        """
        Called when a submission is detected in the message queue
        :param ch: Channel
        :param method:
        :param properties:
        :param body: payload.Grading instance as JSON
        :return:
        """
        # Create submission object from message JSON
        submission = Submission.model_validate_json(body)

        if submission.submission_id == 0:
            # Not really a submission, but a keepalive and test ping
            # Create a dummy result and forward to results queue
            log.info("Ping received, forwarding")
            payload = Grading(
                submission_id=0,
                course_id=0,
                timestamp=submission.timestamp,
                results=b"",
            )
        else:
            # For real submissions, we create a grading job
            log.info(f"Received submission {submission.submission_id}")
            # Attach submission to a grading job instance
            job = Job(submission=submission)

            # Start grading
            worker.grade(job)
            # and read the result, it's a 'Grading' object like the mockup above
            payload = job.grading

        # Return grading result to results queue
        push_result(payload)
        # Grading job done, acknowledge to queue
        ch.basic_ack(delivery_tag=method.delivery_tag)
        if not consume_messages:
            unsubscribe()

    # subscribe only if messages may be processed by this instance
    if consume_messages:
        subscribe_jobs(callback)
        if stay_connected == "on":
            while True and consume_messages:
                subscribe_jobs(callback)
    else:
        unsubscribe()


if __name__ == "__main__":
    main()
