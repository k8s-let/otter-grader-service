import os

# General
loglevel = os.getenv("LOGLEVEL", "INFO")
deployment_type = os.getenv("DEPLOYMENT_TYPE", "dev")  # queue and user suffices
cert_directory = os.getenv("CERT_DIRECTORY", "/etc/rabbitmq/certs")

# Queue connection parameters
rabbitmq_host = os.getenv("RABBITMQ_HOST", "localhost")
rabbitmq_virtualhost = os.getenv(
    "RABBITMQ_VIRTUALHOST", f"ottergrader-{deployment_type}"
)
rabbitmq_user = os.getenv("RABBITMQ_USER", f"otter-{deployment_type}")
rabbitmq_password = os.getenv("RABBITMQ_PASSWORD")
stay_connected = os.getenv(
    "STAY_CONNECTED", "off"
)  # retry connections if queue not available
rabbitmq_cert_file = os.getenv(
    "RABBITMQ_CERT_FILE", "cert_otter-client-dev.ethz.ch.pem"
)
rabbitmq_key_file = os.getenv("RABBITMQ_KEY_FILE", "otter-client-dev.ethz.ch.key")
rabbitmq_ca_file = os.getenv("RABBITMQ_CA_FILE", "ETHZ_CA_chain.pem")

# Server/Responder
ttl = os.getenv("TTL", 10)
key = os.getenv("AUTH_KEY")
secret = os.getenv("AUTH_SECRET")
get_token = os.getenv("GET_TOKEN")

# Grader
docker_cmd = os.getenv("DOCKER_CMD", "docker")
grading_home = os.getenv("GRADING_HOME", "/home/jovyan")
grading_image = os.getenv(
    "GRADING_IMAGE", "registry.ethz.ch/k8s-let/notebooks/jh-notebook-universal:3.0.0-36"
)

# Responder
moodle_host = os.getenv("MOODLE_HOST", "localhost")
push_token = os.getenv("PUSH_TOKEN")
