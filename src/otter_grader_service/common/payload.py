import time
from pydantic import BaseModel
from fastapi import HTTPException

from .logging import logger
from .configuration import ttl, get_token
from .checksum import calc_md5, calc_signature


log = logger(__name__)


class Payload(BaseModel):
    """
    Model of the base payload part of the HTTP request
    """

    rc: int = 0
    status: str = "OK"
    submission_id: int
    course_id: int

    timestamp: float = 0.0  # internally added, to monitor roundtrip time

    def md5_sum(self):
        return calc_md5(self)


class Submission(Payload):
    """
    Payload for submissions (incoming stuff)
    """

    solution: str
    submission: str


class Grading(Payload):
    """
    Payload for grading results (outgoing stuff)
    """

    results: bytes


class SecureMessage(BaseModel):
    """
    Represents the data transferred by HTTP requests, payload and data necessary to validate authentication
    and integrity.
    """

    payload: Payload
    md5_payload: str
    auth: str
    timestamp: int
    rc: int = 0
    msg: str = "OK"

    def validate_checksum(self):
        """
        Validate integrity of data submitted. Compares the checksum calculated from the payload with
        the checksum submitted.

        Raises a fastapi.HTTPException on failure.
        :return: True if check has passed
        """
        if self.md5_payload.encode("utf-8") != self.payload.md5_sum():
            raise HTTPException(
                status_code=500,
                detail={"return": {"code": 500, "status": "Checksum validation error"}},
            )
        return True

    def validate_timestamp(self):
        """
        Validates the timestamp submitted against TTL set in configuration to avoid offline attacks.

        Raises a fastapi.HTTPException on failure.
        :return: None
        """
        if time.time() - float(self.timestamp) >= float(ttl):
            raise HTTPException(
                status_code=500,
                detail={
                    "return": {"code": 500, "status": "Timestamp validation error"}
                },
            )
        return True

    def validate_secret(self):
        """
        Authenticates the request by challenge/response.
        :return: None
        """
        if self.auth != calc_signature(str(self.timestamp), self.md5_payload):
            raise HTTPException(
                status_code=500,
                detail={"return": {"code": 500, "status": "Authentication error"}},
            )

    def validate_all(self):
        """
        Perform all validations: timestamp, checksum and authentication. Raises a fastapi.HTTPException on failure.
        :return: None
        """
        self.validate_timestamp()
        self.validate_checksum()
        self.validate_secret()


class TokenMessage(BaseModel):
    """
    Message for simple token based authentication.
    """

    payload: Payload
    token: str
    rc: int = 0
    msg: str = "OK"

    def validate_all(self):
        if self.token == get_token:
            return True
        else:
            return False
